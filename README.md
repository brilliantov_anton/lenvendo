# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Before Commit

1) Run CS FIXER - php-cs-fixer fix
2) Run PhpStan - vendor/bin/phpstan analyse


### Run As

```bash
anton_brilliantov@~/Source/lenvendo-test-job$ docker exec -it    lenvendo-php   php /app/example/app.php echo [param1=ssdf] [param2={v1,v2,v3}] {log}
Run echo command.
> Arguments: {"log":true}
> Options: {"param1":"ssdf","param2":["v1","v2","v3"]}
```


```bash
anton_brilliantov@~/Source/lenvendo-test-job$ docker exec -it    lenvendo-php   php /app/example/app.php echo [param1=ssdf] [param2={v1,v2,v3}] {log} {help} [paginate={50}]
Called command: echo
	description: echo command description
	help: echo command help

Arguments:
	 - log
	 - help

Options:
	 - param1
		 - ssdf
	 - param2
		 - v1
		 - v2
		 - v3
	 - paginate
		 - 50
```