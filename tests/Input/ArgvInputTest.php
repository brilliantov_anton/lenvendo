<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests\Input;

use Lenvendo\Console\Input\ArgvInput;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertSame;
use function PHPUnit\Framework\assertTrue;
use PHPUnit\Framework\TestCase;

/**
 * Arguments:
 *  -  verbose
 *  -  overwrite
 *  -  unlimited
 *  -  log
 * Options:
 *  -  log_file
 *      -  app.log
 *  -  methods
 *      -  create
 *      -  update
 *      -  delete
 *  -  paginate
 *      -  50.
 */
class ArgvInputTest extends TestCase
{
    private const INPUT = [
        'cli.php',
        'command_name',
        '{verbose,overwrite}',
        '[log_file=app.log]',
        '{unlimited}',
        '[methods={create,update,delete}]',
        '[paginate=50]',
        '{log}',
    ];

    public function testArguments(): void
    {
        $input = new ArgvInput(self::INPUT);

        assertTrue($input->agruments()->has('verbose'));
        assertTrue($input->agruments()->has('overwrite'));
        assertTrue($input->agruments()->has('unlimited'));
        assertTrue($input->agruments()->has('log'));

        assertFalse($input->agruments()->has('z'));
    }

    public function testOptions(): void
    {
        $input = new ArgvInput(self::INPUT);

        assertTrue($input->options()->has('log_file'));
        assertTrue($input->options()->has('methods'));
        assertTrue($input->options()->has('paginate'));
        assertFalse($input->options()->has('x'));

        assertSame('app.log', $input->options()->get('log_file'));
        assertSame(
            [
                'create',
                'update',
                'delete',
            ],
            $input->options()->get('methods')
        );
        assertSame(
            '50',
            $input->options()->get('paginate')
        );
    }

    public function testCommandName(): void
    {
        $input = new ArgvInput(self::INPUT + [' sdfdf']);

        assertSame('command_name', $input->commandName());
    }

    public function testCliArgv(): void
    {
        $input = new ArgvInput(
            [
                'cli.app',
                'echo',
                '[param1=ssdf]',
                '[param2=v1]',
                '[param2=v2]',
                '[param2=v3]',
                '{log}',
            ]
        );

        assertSame('echo', $input->commandName());
        assertTrue($input->agruments()->has('log'));
        assertSame('ssdf', $input->options()->get('param1'));
        assertSame(['v1', 'v2', 'v3'], $input->options()->get('param2'));
    }
}