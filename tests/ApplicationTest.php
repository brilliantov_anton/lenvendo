<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests;

use Lenvendo\Console\Application;
use Lenvendo\Console\Command\CommandCollectionInterface;
use Lenvendo\Console\Command\CommandInterface;
use Lenvendo\Console\Context\ContextInterface;
use Lenvendo\Console\Exception\CommandNameIsUnderfinedException;
use Lenvendo\Console\Exception\CommandNotFoundException;
use Lenvendo\Console\Exception\RuntimeException;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;
use function PHPUnit\Framework\assertSame;
use PHPUnit\Framework\TestCase;

class ApplicationTest extends TestCase
{
    private CommandCollectionInterface $commandCollection;
    private Application                $application;
    private InputInterface             $input;
    private OutputInterface            $output;

    protected function setUp(): void
    {
        parent::setUp();

        $this->commandCollection = $this->createMock(CommandCollectionInterface::class);

        $this->application = new Application($this->commandCollection);

        $this->input = $this->createMock(InputInterface::class);
        $this->output = $this->createMock(OutputInterface::class);
    }

    public function testCommandNameIsUnderfinedException(): void
    {
        $this->input->expects(self::once())->method('commandName')
            ->willThrowException(new CommandNameIsUnderfinedException([]));

        $inputContext = $this->createMock(ContextInterface::class);
        $inputContext->expects(self::once())->method('delete')->with('help');

        $this->input->expects(self::once())
            ->method('agruments')
            ->willReturn($inputContext);

        $listCommand = $this->createMock(CommandInterface::class);
        $listCommand->expects(self::once())->method('run')
            ->with($this->input, $this->output);

        $this->commandCollection->expects(self::once())
            ->method('get')
            ->with('list')
            ->willReturn($listCommand);

        $this->application->run($this->input, $this->output);
    }

    public function testCommandNotFoundException(): void
    {
        $this->input->expects(self::once())
            ->method('commandName')
            ->willReturn('zzz');

        $inputContext = $this->createMock(ContextInterface::class);
        $inputContext->expects(self::once())->method('delete')->with('help');

        $this->input->expects(self::once())
            ->method('agruments')
            ->willReturn($inputContext);

        $listCommand = $this->createMock(CommandInterface::class);
        $listCommand->expects(self::once())->method('run')
            ->with($this->input, $this->output);

        $this->commandCollection->expects(self::exactly(2))
            ->method('get')
            ->willReturnCallback(static function (string $command) use ($listCommand) {
                return $command === 'list' ? $listCommand : throw new CommandNotFoundException();
            });

        $this->application->run($this->input, $this->output);
    }

    public function testRuntimeException(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('ZZZCommand Runtime Exception.');

        $this->input->expects(self::once())
            ->method('commandName')
            ->willReturn('zzz');

        $zzzCommand = $this->createMock(CommandInterface::class);

        $zzzCommand->expects(self::once())->method('run')
            ->with($this->input, $this->output)
            ->willThrowException(new RuntimeException('ZZZCommand Runtime Exception.'));

        $this->commandCollection->expects(self::once())
            ->method('get')
            ->with('zzz')
            ->willReturn($zzzCommand);

        $this->application->run($this->input, $this->output);
    }

    public function testPostive(): void
    {
        $this->input->expects(self::once())
            ->method('commandName')
            ->willReturn('zzz');

        $zzzCommand = $this->createMock(CommandInterface::class);

        $zzzCommand->expects(self::once())->method('run')
            ->with($this->input, $this->output)
            ->willReturn(CommandInterface::SUCCESS);

        $this->commandCollection->expects(self::once())
            ->method('get')
            ->with('zzz')
            ->willReturn($zzzCommand);

        assertSame(CommandInterface::SUCCESS, $this->application->run($this->input, $this->output));
    }
}