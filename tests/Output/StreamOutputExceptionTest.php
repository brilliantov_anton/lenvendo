<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests\Output;

use Lenvendo\Console\Exception\InvalidArgumentException;
use Lenvendo\Console\Output\StreamOutput;
use PHPUnit\Framework\TestCase;

class StreamOutputExceptionTest extends TestCase
{
    public function testException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The StreamOutput class needs a stream as its first argument.');

        new StreamOutput('');
    }
}