<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests\Output;

use const DIRECTORY_SEPARATOR;
use Lenvendo\Console\Output\StreamOutput;
use const PHP_EOL;
use function PHPUnit\Framework\assertFileDoesNotExist;
use function PHPUnit\Framework\assertFileExists;
use function PHPUnit\Framework\assertSame;
use PHPUnit\Framework\TestCase;

class StreamOutputTest extends TestCase
{
    private StreamOutput $output;
    private string       $streamPath;

    protected function setUp(): void
    {
        parent::setUp();

        $this->streamPath = implode(
            DIRECTORY_SEPARATOR,
            [
                __DIR__,
                'Result',
                'stream.log',
            ]
        );

        @unlink($this->streamPath);

        $this->output = new StreamOutput(fopen($this->streamPath, 'a'));

        assertFileExists($this->streamPath);
    }

    protected function tearDown(): void
    {
        @unlink($this->streamPath);

        assertFileDoesNotExist($this->streamPath);

        parent::tearDown();
    }

    public function testWriteln(): void
    {
        $this->output->writeln('row1');
        $this->output->writeln('row2');
        $this->output->writeln('row3');

        assertSame(
            implode(
                '',
                [
                    'row1' . PHP_EOL,
                    'row2' . PHP_EOL,
                    'row3' . PHP_EOL,
                ],
            ),
            file_get_contents($this->streamPath)
        );
    }
}