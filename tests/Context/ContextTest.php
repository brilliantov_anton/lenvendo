<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests\Context;

use Lenvendo\Console\Context\Context;
use function PHPUnit\Framework\assertEmpty;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertNull;
use function PHPUnit\Framework\assertSame;
use function PHPUnit\Framework\assertTrue;
use PHPUnit\Framework\TestCase;

class ContextTest extends TestCase
{
    private Context $context;

    protected function setUp(): void
    {
        parent::setUp();

        $this->context = new Context();
    }

    public function testNullGet(): void
    {
        assertNull($this->context->get('key'));
    }

    public function testSetAndGet(): void
    {
        $this->context->set('zzz', 'xxx');

        assertSame('xxx', $this->context->get('zzz'));
    }

    public function testFill(): void
    {
        assertNull($this->context->get('xxx'));

        $this->context->fill(['xxx' => 'ccc']);

        assertSame('ccc', $this->context->get('xxx'));
    }

    public function testHas(): void
    {
        assertFalse($this->context->has('zzz'));

        $this->context->set('zzz', 1);

        assertTrue($this->context->has('zzz'));
    }

    public function testIssetFalse(): void
    {
        $this->context->set('z', null);

        assertFalse($this->context->isset('z'));
    }

    public function testIssetTrue(): void
    {
        $this->context->set('z', 1);

        assertTrue($this->context->isset('z'));
    }

    public function testDelete(): void
    {
        $this->context->set('zzz', 1);

        assertTrue($this->context->has('zzz'));

        $this->context->delete('zzz');

        assertFalse($this->context->has('zzz'));
    }

    public function testAll(): void
    {
        $this->context->fill(
            [
                'z' => 1,
                'x' => 2,
            ]
        );

        assertSame(
            [
                'z' => 1,
                'x' => 2,
            ],
            $this->context->all()
        );
    }

    public function testFillAndClear(): void
    {
        $this->context->fill(
            [
                'z' => 1,
                'x' => 2,
            ]
        );

        assertSame(
            [
                'z' => 1,
                'x' => 2,
            ],
            $this->context->all()
        );

        $this->context->fill([]);

        assertEmpty($this->context->all());
    }
}