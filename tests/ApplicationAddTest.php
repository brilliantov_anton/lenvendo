<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests;

use Lenvendo\Console\Application;
use Lenvendo\Console\Command\CommandCollectionInterface;
use Lenvendo\Console\Tests\Command\Fixtutes\SuccessCommand;
use PHPUnit\Framework\TestCase;

class ApplicationAddTest extends TestCase
{
    private CommandCollectionInterface $commandCollection;
    private Application                $application;

    protected function setUp(): void
    {
        parent::setUp();

        $this->commandCollection = $this->createMock(CommandCollectionInterface::class);
        $this->application = new Application($this->commandCollection);
    }

    public function testAdd(): void
    {
        $command = new SuccessCommand();

        $this->commandCollection->expects(self::once())->method('add')->with($command);

        $this->application->add($command);
    }
}