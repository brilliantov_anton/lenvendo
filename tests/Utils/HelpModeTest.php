<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests\Utils;

use Lenvendo\Console\Command\CommandInterface;
use Lenvendo\Console\Context\Context;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;
use Lenvendo\Console\Utils\HelpMode;
use PHPUnit\Framework\TestCase;

class HelpModeTest extends TestCase
{
    private OutputInterface $output;
    private InputInterface  $input;

    protected function setUp(): void
    {
        parent::setUp();

        $this->output = $this->createMock(OutputInterface::class);
        $this->input = $this->createMock(InputInterface::class);
    }

    public function testHelpMode(): void
    {
        $command = $this->createMock(CommandInterface::class);
        $command->expects(self::once())->method('name')->willReturn('command_name');
        $command->expects(self::once())->method('description')->willReturn('command_description');
        $command->expects(self::once())->method('help')->willReturn('command_help');

        $helpMode = new HelpMode($this->output);

        $agruments = new Context();
        $agruments->set('z', true);
        $agruments->set('x', true);

        $this->input->expects(self::once())->method('agruments')
            ->willReturn($agruments);

        $options = new Context();
        $options->set('zzz', 1);
        $options->set(
            'xxx',
            [
                3,
                4,
            ]
        );

        $this->input->expects(self::once())->method('options')
            ->willReturn($options);

        $this->output->expects(self::exactly(14))
            ->method('writeln')
            ->willReturnOnConsecutiveCalls(
                'Called command: command_name',
                '	description: command_description',
                '	help: command_help',
                '',
                'Arguments:',
                '',
                '	 - z',
                '	 - x',
                'Options:',
                '	 - zzz',
                '		 - 1',
                '	 - xxx',
                '		 - 3',
                '		 - 4',
            );

        $helpMode($command, $this->input);
    }
}