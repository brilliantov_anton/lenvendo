<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests\Command;

use Lenvendo\Console\Exception\CommandAttributeNotImplementedException;
use Lenvendo\Console\Tests\Command\Fixtutes\ExceptionCommand;
use Lenvendo\Console\Tests\Command\Fixtutes\SuccessCommand;
use function PHPUnit\Framework\assertSame;
use PHPUnit\Framework\TestCase;

class AbtsractCommandTest extends TestCase
{
    public function testCommandAttributeNotImplementedException(): void
    {
        $this->expectException(CommandAttributeNotImplementedException::class);
        $this->expectExceptionMessage(
            'Class "Lenvendo\Console\Tests\Command\Fixtutes\ExceptionCommand" not configured AsCommand Attribute.'
        );

        new ExceptionCommand();
    }

    public function testSucess(): void
    {
        $command = new SuccessCommand();

        assertSame('success-command', $command->name());
        assertSame('success-command-description', $command->description());
        assertSame('This is "success-command" help;', $command->help());
    }
}