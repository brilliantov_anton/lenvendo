<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests\Command;

use Lenvendo\Console\Command\CommandCollectionInterface;
use Lenvendo\Console\Command\CommandInterface;
use Lenvendo\Console\Command\ListCommand;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;
use function PHPUnit\Framework\assertSame;
use PHPUnit\Framework\TestCase;

class ListCommandTest extends TestCase
{
    private CommandCollectionInterface $commandCollection;
    private ListCommand                $command;

    protected function setUp(): void
    {
        parent::setUp();

        $this->commandCollection = $this->createMock(CommandCollectionInterface::class);
        $this->command = new ListCommand($this->commandCollection);
    }

    public function testName(): void
    {
        assertSame('list', $this->command->name());
    }

    public function testDescription(): void
    {
        assertSame('list of all commands', $this->command->description());
    }

    public function testHelp(): void
    {
        assertSame('Show all command list.', $this->command->help());
    }

    public function testCommand(): void
    {
        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);

        /**
         * @var CommandInterface[] $commands
         */
        $commands = [
            $this->command('cmd1', 'sdfsdf1'),
            $this->command('cmd2', 'sdfsdf2'),
        ];

        $this->commandCollection->expects(self::once())->method('all')
            ->willReturnCallback(static function () use ($commands) {
                yield $commands[0];

                yield $commands[1];
            });

        $output->expects(self::exactly(2))->method('writeln')
            ->willReturnOnConsecutiveCalls(
                [
                    ' > cmd1 | sdfsdf1',
                    ' > cmd2 | sdfsdf2',
                ]
            );

        $result = $this->command->run($input, $output);

        assertSame(CommandInterface::SUCCESS, $result);
    }

    private function command(string $name, string $description): CommandInterface
    {
        $result = $this->createMock(CommandInterface::class);
        $result->expects(self::once())->method('name')->willReturn($name);
        $result->expects(self::once())->method('description')->willReturn($description);

        return $result;
    }
}