<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests\Command\Fixtutes;

use Lenvendo\Console\Attribute\AsCommand;
use Lenvendo\Console\Command\AbstractCommand;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;

#[AsCommand(
    name: 'success-command',
    description: 'success-command-description'
)]
class SuccessCommand extends AbstractCommand
{
    protected function configure(): void
    {
    }

    public function help(): string
    {
        return <<<TXT
            This is "success-command" help;
            TXT;
    }

    protected function call(InputInterface $input, OutputInterface $output): int
    {
        return self::SUCCESS;
    }
}