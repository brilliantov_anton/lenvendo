<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests\Command\Fixtutes;

use Lenvendo\Console\Command\AbstractCommand;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;

class ExceptionCommand extends AbstractCommand
{
    protected function configure(): void
    {
    }

    public function help(): string
    {
        return '';
    }

    protected function call(InputInterface $input, OutputInterface $output): int
    {
        return self::SUCCESS;
    }
}