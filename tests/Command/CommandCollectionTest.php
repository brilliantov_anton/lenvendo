<?php

declare(strict_types=1);

namespace Lenvendo\Console\Tests\Command;

use Lenvendo\Console\Command\CommandCollection;
use Lenvendo\Console\Exception\CommandNotFoundException;
use Lenvendo\Console\Tests\Command\Fixtutes\SuccessCommand;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;
use PHPUnit\Framework\TestCase;

class CommandCollectionTest extends TestCase
{
    private CommandCollection $collection;

    protected function setUp(): void
    {
        parent::setUp();

        $this->collection = new CommandCollection();
    }

    public function testCommandNotFoundException(): void
    {
        $this->expectException(CommandNotFoundException::class);
        $this->expectExceptionMessage('Command "zzz" not found.');

        $this->collection->get('zzz');
    }

    public function testCommandNotExist(): void
    {
        assertFalse($this->collection->has('zzz'));
    }

    public function testFullPositiveScenario(): void
    {
        $command = new SuccessCommand();

        $this->collection->add($command);

        assertTrue($this->collection->has('success-command'));
        assertEquals($command, $this->collection->get('success-command'));

        assertEquals(
            [$command],
            iterator_to_array($this->collection->all())
        );
    }
}