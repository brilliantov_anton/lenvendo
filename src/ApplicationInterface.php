<?php

declare(strict_types=1);

namespace Lenvendo\Console;

use Lenvendo\Console\Command\CommandInterface;
use Lenvendo\Console\Exception\RuntimeException;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;

interface ApplicationInterface
{
    public function add(CommandInterface $command): void;

    /**
     * @throws RuntimeException
     */
    public function run(InputInterface $input = null, OutputInterface $output = null): int;
}