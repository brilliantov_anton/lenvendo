<?php

declare(strict_types=1);

namespace Lenvendo\Console\Input;

use Lenvendo\Console\Context\ContextInterface;
use Lenvendo\Console\Exception\CommandNameIsUnderfinedException;

/**
 * Arguments:
 *  -  verbose
 *  -  overwrite
 *  -  unlimited
 *  -  log
 * Options:
 *  -  log_file
 *      -  app.log
 *  -  methods
 *      -  create
 *      -  update
 *      -  delete
 *  -  paginate
 *      -  50.
 */
interface InputInterface
{
    /**
     * @todo возможно метод надо убрать и хранить значение в Options
     *
     * @throws CommandNameIsUnderfinedException
     */
    public function commandName(): string;

    public function agruments(): ContextInterface;

    public function options(): ContextInterface;
}