<?php

declare(strict_types=1);

namespace Lenvendo\Console\Input;

use Lenvendo\Console\Context\Context;
use Lenvendo\Console\Context\ContextInterface;
use Lenvendo\Console\Exception\CommandNameIsUnderfinedException;

/**
 * Arguments:
 *  -  verbose
 *  -  overwrite
 *  -  unlimited
 *  -  log
 * Options:
 *  -  log_file
 *      -  app.log
 *  -  methods
 *      -  create
 *      -  update
 *      -  delete
 *  -  paginate
 *      -  50.
 *
 * @todo add type casting
 */
class ArgvInput implements InputInterface
{
    private ?string          $commandName = null;
    private ContextInterface $agruments;
    private ContextInterface $options;
    //
    private array $tokens;
    private array $parsedTokens;
    //
    private bool $parsed = false;

    public function __construct(array $argv = null)
    {
        $argv ??= $_SERVER['argv'] ?? [];

        // strip the application name
        array_shift($argv);

        $this->tokens = $argv;

        $this->agruments = new Context();
        $this->options = new Context();
    }

    public function commandName(): string
    {
        $this->parse();

        if ($this->commandName === null) {
            throw new CommandNameIsUnderfinedException($this->tokens);
        }

        return $this->commandName;
    }

    public function agruments(): ContextInterface
    {
        $this->parse();

        return $this->agruments;
    }

    public function options(): ContextInterface
    {
        $this->parse();

        return $this->options;
    }

    private function parse(): void
    {
        if ($this->parsed) {
            return;
        }

        $this->parsedTokens = $this->tokens;

        $count = 0;

        while (($token = array_shift($this->parsedTokens)) !== null) {
            ++$count;

            if ($this->isArguments($token)) {
                $this->parseArguments($token);
            } elseif (str_starts_with($token, '[') && str_ends_with($token, ']')) {
                $this->parseOptions($token);
            } elseif ($count === 1 && $this->commandName === null) {
                $this->commandName = $token;
            }
        }

        $this->parsed = true;
    }

    private function parseArguments(string $arguments): void
    {
        foreach ($this->prepareArguments($arguments) as $argument) {
            $this->agruments->set($argument, true);
        }
    }

    private function parseOptions(string $options): void
    {
        $options = $this->clearRow($options);

        if (!str_contains($options, '=')) {
            //todo add error?
            return;
        }

        [
            $name,
            $value,
        ] = explode('=', $options, 2);

        if ($this->isArguments($value)) {
            $value = $this->prepareArguments($value);
        }

        if ($this->options->empty($name)) {
            $this->options->set($name, $value);
        } else {
            $value = array_merge(
                (array) $this->options->get($name),
                (array) $value
            );

            $this->options->set($name, $value);
        }
    }

    private function prepareArguments(string $arguments): array
    {
        return array_map(
            static fn (string $argument) => trim($argument),
            explode(',', $this->clearRow($arguments))
        );
    }

    private function isArguments(string $input): bool
    {
        return str_starts_with($input, '{') && str_ends_with($input, '}');
    }

    private function clearRow(string $input): string
    {
        return substr($input, 1, -1);
    }
}