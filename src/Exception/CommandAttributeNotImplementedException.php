<?php

declare(strict_types=1);

namespace Lenvendo\Console\Exception;

class CommandAttributeNotImplementedException extends RuntimeException
{
}