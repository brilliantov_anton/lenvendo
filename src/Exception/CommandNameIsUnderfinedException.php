<?php

declare(strict_types=1);

namespace Lenvendo\Console\Exception;

use Throwable;

class CommandNameIsUnderfinedException extends RuntimeException
{
    public function __construct(private array $input, string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function input(): array
    {
        return $this->input;
    }
}