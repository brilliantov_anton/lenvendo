<?php

declare(strict_types=1);

namespace Lenvendo\Console\Exception;

use Throwable;

interface ExceptionInterface extends Throwable
{
}