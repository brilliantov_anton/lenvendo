<?php

declare(strict_types=1);

namespace Lenvendo\Console\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}