<?php

declare(strict_types=1);

namespace Lenvendo\Console\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}