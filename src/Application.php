<?php

declare(strict_types=1);

namespace Lenvendo\Console;

use Lenvendo\Console\Command\CommandCollectionInterface;
use Lenvendo\Console\Command\CommandInterface;
use Lenvendo\Console\Command\ListCommand;
use Lenvendo\Console\Exception\CommandNameIsUnderfinedException;
use Lenvendo\Console\Exception\CommandNotFoundException;
use Lenvendo\Console\Exception\RuntimeException;
use Lenvendo\Console\Input\ArgvInput;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\ConsoleOutput;
use Lenvendo\Console\Output\OutputInterface;
use Throwable;

class Application implements ApplicationInterface
{
    public function __construct(private CommandCollectionInterface $commandCollection)
    {
        //todo возможно стоит вынести в метод
        $this->commandCollection->add(new ListCommand($this->commandCollection));
    }

    public function add(CommandInterface $command): void
    {
        $this->commandCollection->add($command);
    }

    public function run(InputInterface $input = null, OutputInterface $output = null): int
    {
        if ($input === null) {
            $input = new ArgvInput();
        }

        if ($output === null) {
            $output = new ConsoleOutput();
        }

        try {
            $this->commandCollection->get($input->commandName())->run($input, $output);
        } catch (CommandNameIsUnderfinedException|CommandNotFoundException) {
            $input->agruments()->delete('help');

            $this->commandCollection->get('list')->run($input, $output);
        } catch (Throwable $exception) {
            throw new RuntimeException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return 0;
    }
}