<?php

declare(strict_types=1);

namespace Lenvendo\Console\Context;

interface ContextInterface
{
    public function set(string $key, mixed $value): void;

    public function get(string $key): mixed;

    public function fill(array $array): void;

    public function has(string $key): bool;

    public function isset(string $key): bool;

    public function delete(string $key): void;

    public function empty(string $key): bool;

    public function all(): array;
}