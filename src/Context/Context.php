<?php

declare(strict_types=1);

namespace Lenvendo\Console\Context;

use function array_key_exists;

class Context implements ContextInterface
{
    private array $context;

    public function __construct()
    {
        $this->context = [];
    }

    public function set(string $key, mixed $value): void
    {
        $this->context[$key] = $value;
    }

    public function get(string $key): mixed
    {
        return $this->context[$key] ?? null;
    }

    public function fill(array $array): void
    {
        $this->context = $array;
    }

    public function has(string $key): bool
    {
        return array_key_exists($key, $this->context);
    }

    public function isset(string $key): bool
    {
        return isset($this->context[$key]);
    }

    public function delete(string $key): void
    {
        unset($this->context[$key]);
    }

    public function empty(string $key): bool
    {
        return empty($this->context[$key]);
    }

    public function all(): array
    {
        return $this->context;
    }
}