<?php

declare(strict_types=1);

namespace Lenvendo\Console\Utils;

use Lenvendo\Console\Command\CommandInterface;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;

//todo подумать над названием
class HelpMode
{
    public function __construct(private OutputInterface $output)
    {
    }

    public function __invoke(CommandInterface $command, InputInterface $input): void
    {
        $this->output->writeln("Called command: {$command->name()}");
        $this->output->writeln("\tdescription: {$command->description()}");
        $this->output->writeln("\thelp: {$command->help()}");

        $this->output->writeln('');
        $this->output->writeln('Arguments:');

        foreach (array_keys($input->agruments()->all()) as $key) {
            $this->output->writeln("\t - {$key}");
        }

        $this->output->writeln('');
        $this->output->writeln('Options:');

        foreach ($input->options()->all() as $name => $values) {
            $this->output->writeln("\t - {$name}");

            if (!is_iterable($values)) {
                $values = [$values];
            }

            foreach ($values as $value) {
                $this->output->writeln("\t\t - {$value}");
            }
        }
    }
}