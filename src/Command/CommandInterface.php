<?php

declare(strict_types=1);

namespace Lenvendo\Console\Command;

use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;

interface CommandInterface
{
    // see https://tldp.org/LDP/abs/html/exitcodes.html
    public const SUCCESS = 0;
    public const FAILURE = 1;
    public const INVALID = 2;

    public function name(): string;

    public function description(): string;

    public function help(): string;

    public function run(InputInterface $input, OutputInterface $output): int;
}