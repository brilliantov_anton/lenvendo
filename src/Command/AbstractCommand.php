<?php

declare(strict_types=1);

namespace Lenvendo\Console\Command;

use Lenvendo\Console\Attribute\AsCommand;
use Lenvendo\Console\Exception\CommandAttributeNotImplementedException;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;
use Lenvendo\Console\Utils\HelpMode;
use ReflectionAttribute;
use ReflectionClass;

abstract class AbstractCommand implements CommandInterface
{
    private string $name;
    private string $description;

    public function __construct()
    {
        $this->ensureAsCommandAttribute();
    }

    public function name(): string
    {
        return $this->name;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        if ($input->agruments()->has('help')) {
            $this->runHelpMode($input, $output);

            return self::SUCCESS;
        }

        return $this->call($input, $output);
    }

    private function runHelpMode(InputInterface $input, OutputInterface $output): void
    {
        $helpMode = new HelpMode($output);
        $helpMode($this, $input);
    }

    abstract protected function call(InputInterface $input, OutputInterface $output): int;

    abstract protected function configure(): void;

    private function ensureAsCommandAttribute(): void
    {
        $attributes = (new ReflectionClass(static::class))
            ->getAttributes(AsCommand::class, ReflectionAttribute::IS_INSTANCEOF);

        if (empty($attributes)) {
            throw new CommandAttributeNotImplementedException(sprintf('Class "%s" not configured AsCommand Attribute.', static::class));
        }

        /**
         * @var AsCommand $asCommand
         */
        $asCommand = $attributes[0]->newInstance();

        $this->configureProperties($asCommand);
    }

    private function configureProperties(AsCommand $asCommandAttribute): void
    {
        $this->name = $asCommandAttribute->name();
        $this->description = $asCommandAttribute->description();
    }
}