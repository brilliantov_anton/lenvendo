<?php

declare(strict_types=1);

namespace Lenvendo\Console\Command;

use Generator;
use Lenvendo\Console\Exception\CommandNotFoundException;

interface CommandCollectionInterface
{
    public function add(CommandInterface $command): void;

    public function has(string $name): bool;

    /**
     * @throws CommandNotFoundException
     */
    public function get(string $name): CommandInterface;

    /**
     * @return Generator<CommandInterface>
     */
    public function all(): Generator;
}