<?php

declare(strict_types=1);

namespace Lenvendo\Console\Command;

use Lenvendo\Console\Attribute\AsCommand;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;

#[AsCommand(
    name: 'list',
    description: 'list of all commands'
)]
class ListCommand extends AbstractCommand
{
    public function __construct(private CommandCollectionInterface $commandCollection)
    {
        parent::__construct();
    }

    protected function call(InputInterface $input, OutputInterface $output): int
    {
        /**
         * @var CommandInterface $command
         */
        foreach ($this->commandCollection->all() as $command) {
            if (!$command instanceof self) {
                $output->writeln(" > {$command->name()} | {$command->description()}");
            }
        }

        return self::SUCCESS;
    }

    protected function configure(): void
    {
    }

    public function help(): string
    {
        return <<<TXT
            Show all command list.
            TXT;
    }
}