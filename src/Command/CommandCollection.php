<?php

declare(strict_types=1);

namespace Lenvendo\Console\Command;

use Generator;
use Lenvendo\Console\Exception\CommandNotFoundException;

class CommandCollection implements CommandCollectionInterface
{
    /**
     * @var array<string, CommandInterface>
     */
    private array $commands;

    public function __construct()
    {
        $this->commands = [];
    }

    public function add(CommandInterface $command): void
    {
        $this->commands[$command->name()] = $command;
    }

    public function has(string $name): bool
    {
        return isset($this->commands[$name]);
    }

    public function get(string $name): CommandInterface
    {
        return $this->commands[$name]
            ?? throw new CommandNotFoundException(sprintf('Command "%s" not found.', $name));
    }

    public function all(): Generator
    {
        foreach ($this->commands as $command) {
            yield $command;
        }
    }
}