<?php

declare(strict_types=1);

namespace Lenvendo\Console\Command\Example;

use JsonException;
use Lenvendo\Console\Attribute\AsCommand;
use Lenvendo\Console\Command\AbstractCommand;
use Lenvendo\Console\Context\ContextInterface;
use Lenvendo\Console\Input\InputInterface;
use Lenvendo\Console\Output\OutputInterface;

#[AsCommand(
    name: 'echo',
    description: 'echo command description'
)]
class EchoCommand extends AbstractCommand
{
    /**
     * @throws JsonException
     */
    protected function call(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Run echo command.');
        $output->writeln(sprintf(' > Arguments: %s', $this->toString($input->agruments())));
        $output->writeln(sprintf(' > Options: %s', $this->toString($input->options())));

        return self::SUCCESS;
    }

    /**
     * @throws JsonException
     */
    private function toString(ContextInterface $context): string
    {
        return json_encode($context->all(), JSON_THROW_ON_ERROR);
    }

    protected function configure(): void
    {
    }

    public function help(): string
    {
        return 'echo command help';
    }
}