<?php

declare(strict_types=1);

namespace Lenvendo\Console\Output;

use function is_resource;
use Lenvendo\Console\Exception\InvalidArgumentException;
use const PHP_EOL;

/**
 * StreamOutput writes the output to a given stream.
 *
 * Usage:
 *
 *     $output = new StreamOutput(fopen('php://stdout', 'w'));
 *
 * As `StreamOutput` can use any stream, you can also use a file:
 *
 *     $output = new StreamOutput(fopen('/path/to/output.log', 'a', false));
 */
class StreamOutput extends AbstractOutput
{
    /**
     * @param resource $stream
     */
    public function __construct(private $stream)
    {
        if (!is_resource($stream) || get_resource_type($stream) !== 'stream') {
            throw new InvalidArgumentException('The StreamOutput class needs a stream as its first argument.');
        }
    }

    protected function doWrite(string $message, bool $newline): void
    {
        if ($newline) {
            $message .= PHP_EOL;
        }

        fwrite($this->stream, $message);

        fflush($this->stream);
    }
}