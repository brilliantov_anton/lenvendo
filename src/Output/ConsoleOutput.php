<?php

declare(strict_types=1);

namespace Lenvendo\Console\Output;

use Stringable;

class ConsoleOutput implements OutputInterface
{
    private StreamOutput $output;

    public function __construct()
    {
        $this->output = new StreamOutput(
            fopen('php://stdout', 'w') ?: fopen('php://output', 'w')
        );
    }

    public function writeln(iterable|Stringable|string $messages): void
    {
        $this->output->writeln($messages);
    }

    public function write(iterable|Stringable|string $messages, bool $newline = false): void
    {
        $this->output->write($messages, $newline);
    }
}