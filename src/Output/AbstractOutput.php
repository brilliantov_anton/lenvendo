<?php

declare(strict_types=1);

namespace Lenvendo\Console\Output;

use Stringable;

abstract class AbstractOutput implements OutputInterface
{
    public function writeln(iterable|Stringable|string $messages): void
    {
        $this->write($messages, true);
    }

    public function write(iterable|Stringable|string $messages, bool $newline = false): void
    {
        if (!is_iterable($messages)) {
            $messages = [$messages];
        }

        foreach ($messages as $message) {
            $this->doWrite($message, $newline);
        }
    }

    abstract protected function doWrite(string $message, bool $newline): void;
}