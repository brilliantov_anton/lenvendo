<?php

declare(strict_types=1);

namespace Lenvendo\Console\Output;

use Stringable;

class NullOutput implements OutputInterface
{
    public function writeln(iterable|Stringable|string $messages): void
    {
    }

    public function write(iterable|Stringable|string $messages, bool $newline = false): void
    {
    }
}