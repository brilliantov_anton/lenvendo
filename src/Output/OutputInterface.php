<?php

declare(strict_types=1);

namespace Lenvendo\Console\Output;

use Stringable;

interface OutputInterface
{
    public function writeln(string|iterable|Stringable $messages): void;

    public function write(string|iterable|Stringable $messages, bool $newline = false): void;
}