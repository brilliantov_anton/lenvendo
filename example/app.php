<?php

declare(strict_types=1);

use Lenvendo\Console\Application;
use Lenvendo\Console\Command\CommandCollection;
use Lenvendo\Console\Command\Example\EchoCommand;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$application = new Application(new CommandCollection());
$application->add(new EchoCommand());
$application->run();